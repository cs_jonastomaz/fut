//
//  DesafiosListViewController.m
//  fut
//
//  Created by Jonas Tomaz on 3/4/15.
//  Copyright (c) 2015 Jonas Tomaz. All rights reserved.
//

#import "DesafiosListViewController.h"
#import "DesafioListCell.h"
#import "EscolhaDesafioViewController.h"

@interface DesafiosListViewController ()

@property (nonatomic, copy) NSArray *nomes;

@end

@implementation DesafiosListViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nomes = @[@"Treino 2",@"Treino 1"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showRespostas) name:@"ShowRespostasNotification" object:nil];
    [self performSegueWithIdentifier:@"ShowLoginController" sender:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.desafiosCollectionView.delegate = self;
    self.desafiosCollectionView.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DesafioListCell *cell = (DesafioListCell *)[self.desafiosCollectionView dequeueReusableCellWithReuseIdentifier:@"DesafioListCell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
            cell.statusImage.image = [UIImage imageNamed:@"desafio_enviado"];
            cell.nomeDesafio.text = self.nomes[0];
            break;
            
        case 1:
            cell.statusImage.image = nil;
            cell.shadowView.backgroundColor = [UIColor clearColor];
            cell.layer.borderWidth = 3;
            cell.layer.borderColor = [[UIColor greenColor] CGColor];
            cell.nomeDesafio.text = self.nomes[1];
            break;
            
        default:
            cell.statusImage.image = [UIImage imageNamed:@"desafio_feito"];
            cell.shadowView.backgroundColor = [UIColor clearColor];
            break;
    }
    
    NSString *fotoName = [NSString stringWithFormat:@"foto%ld", (long)indexPath.row +1];
    cell.desafioImage.image = [UIImage imageNamed:fotoName];
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [self showBlockDesafioAllert];
            break;
        case 1:
            [self performSegueWithIdentifier:@"ShowDesafioController" sender:indexPath];
            break;
        
        default:
            [self performSegueWithIdentifier:@"ShowRespostasDesafioController" sender:self];
            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *index = sender;
    if ([[segue identifier] isEqualToString:@"ShowDesafioController"]) {
        EscolhaDesafioViewController *ddvc = (EscolhaDesafioViewController *)[segue destinationViewController];
        ddvc.titleName = self.nomes[index.row];
    }
}


-(void)showBlockDesafioAllert
{
    [[[UIAlertView alloc] initWithTitle:@"Passe" message:@"Este desafio será aberto amanhã" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
}

-(void)showRespostas
{
    [self.navigationController popToViewController:self animated:NO];
    [self performSegueWithIdentifier:@"ShowRespostasDesafioController" sender:nil];
}

@end
