//
//  RespostasDesafiosViewController.h
//  fut
//
//  Created by Jonas Tomaz on 3/4/15.
//  Copyright (c) 2015 Jonas Tomaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RespostasDesafiosViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *respostasCollectionView;

@end
