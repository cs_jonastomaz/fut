//
//  RespostasDesafiosViewController.m
//  fut
//
//  Created by Jonas Tomaz on 3/4/15.
//  Copyright (c) 2015 Jonas Tomaz. All rights reserved.
//

#import "RespostasDesafiosViewController.h"
#import "DesafioListCell.h"
#import "RespostasCollectionReusableView.h"

@implementation RespostasDesafiosViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.respostasCollectionView.delegate = self;
    self.respostasCollectionView.dataSource = self;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    RespostasCollectionReusableView *view = [self.respostasCollectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"RespostasCollectionReusableView" forIndexPath:indexPath];
    
    switch (indexPath.section) {
        case 0:
            view.headerLable.text = @"Sua Resposta";
            break;
            
        default:
            view.headerLable.text = @"Veja as respostas da galera";
            break;
    }
    
    return view;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
            
        default:
            return 10;
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return [self desafioListCellAtIndexPath:indexPath];
        }else{
            return [self textoCell:indexPath];
        }
    }else{
        return [self desafioListCellAtIndexPath:indexPath];
    }

}

-(UICollectionViewCell *)textoCell:(NSIndexPath*)indexPath
{
    return [self.respostasCollectionView dequeueReusableCellWithReuseIdentifier:@"textoCell" forIndexPath:indexPath];
}

-(DesafioListCell *)desafioListCellAtIndexPath:(NSIndexPath*)indexPath
{
    DesafioListCell *cell = (DesafioListCell *)[self.respostasCollectionView dequeueReusableCellWithReuseIdentifier:@"DesafioListCell" forIndexPath:indexPath];
    
    switch (indexPath.section) {
        case 0:
            cell.statusImage.image = [UIImage imageNamed:@"desafio_espera"];
            break;
            
        default:
            cell.statusImage.image = [UIImage imageNamed:@"desafio_feito"];
            cell.shadowView.backgroundColor = [UIColor clearColor];
            break;
    }
    
    NSString *fotoName = [NSString stringWithFormat:@"foto%ld", (long)indexPath.row +1];
    cell.desafioImage.image = [UIImage imageNamed:fotoName];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 1) {
        return CGSizeMake(300, 50);
    }else{
        return CGSizeMake(300, 210);
    }
}


@end
