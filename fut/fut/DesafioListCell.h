//
//  DesafioListCell.h
//  fut
//
//  Created by Jonas Tomaz on 3/4/15.
//  Copyright (c) 2015 Jonas Tomaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DesafioListCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *desafioImage;
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *nomeDesafio;

@end
