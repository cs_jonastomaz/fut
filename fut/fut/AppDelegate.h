//
//  AppDelegate.h
//  fut
//
//  Created by Jonas Tomaz on 3/3/15.
//  Copyright (c) 2015 Jonas Tomaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

