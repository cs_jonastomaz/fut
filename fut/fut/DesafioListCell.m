//
//  DesafioListCell.m
//  fut
//
//  Created by Jonas Tomaz on 3/4/15.
//  Copyright (c) 2015 Jonas Tomaz. All rights reserved.
//

#import "DesafioListCell.h"

@implementation DesafioListCell

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.layer.borderWidth = 0;
}

@end
