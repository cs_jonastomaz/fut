//
//  RespostasCollectionReusableView.h
//  fut
//
//  Created by Jonas Tomaz on 3/4/15.
//  Copyright (c) 2015 Jonas Tomaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RespostasCollectionReusableView : UICollectionReusableView
@property (nonatomic, weak) IBOutlet UILabel *headerLable;
@end
