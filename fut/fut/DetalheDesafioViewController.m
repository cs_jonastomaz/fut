//
//  DetalheDesafioViewController.m
//  fut
//
//  Created by Jonas Tomaz on 3/4/15.
//  Copyright (c) 2015 Jonas Tomaz. All rights reserved.
//

#import "DetalheDesafioViewController.h"
#import <MediaPlayer/MediaPlayer.h>

#import <QuartzCore/QuartzCore.h>
#import "AVFoundation/AVFoundation.h"

@interface DetalheDesafioViewController ()
@property (nonatomic, strong) AVPlayer *player;
@property (assign) BOOL fotoTirada;
@end

@implementation DetalheDesafioViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.player && !self.fotoTirada) {
        [self.player play];
    }
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.fotoTirada = NO;
    NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"desafioRobinho" ofType:@"mp4"];
    NSURL *videoURL = [NSURL fileURLWithPath:videoPath];

    self.player = [AVPlayer playerWithURL:videoURL]; //
    
    AVPlayerLayer *layer = [AVPlayerLayer layer];
    
    [layer setPlayer:self.player];
    [layer setFrame:CGRectMake(0, 0, self.playerView.frame.size.width, self.playerView.frame.size.height)];
    [layer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [self.playerView.layer addSublayer:layer];
    [self.player play];
    
}

-(IBAction)actionLaunchAppCamera:(id)sender
{
    
    [self.player pause];
    
    if (self.fromText) {
        [self performSegueWithIdentifier:@"ShowRespostaTexto" sender:self];
    }else{
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePicker.allowsEditing = YES;
            
            [self presentViewController:imagePicker animated:YES completion:^{

            }];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Camera Unavailable"
                                                           message:@"Unable to find a camera on your device."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
            [alert show];
            alert = nil;
        }
    }
}

-(IBAction)backToList:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
//    //This creates a filepath with the current date/time as the name to save the image
//    NSString *presentTimeStamp = [Utilities getPresentDateTime];
//    NSString *fileSavePath = [Utilities documentsPath:presentTimeStamp];
//    fileSavePath = [fileSavePath stringByAppendingString:@".png"];
//    
//    //This checks to see if the image was edited, if it was it saves the edited version as a .png
//    if ([info objectForKey:UIImagePickerControllerEditedImage]) {
//        //save the edited image
//        NSData *imgPngData = UIImagePNGRepresentation([info objectForKey:UIImagePickerControllerEditedImage]);
//        [imgPngData writeToFile:fileSavePath atomically:YES];
//        
//        
//    }else{
//        //save the original image
//        NSData *imgPngData = UIImagePNGRepresentation([info objectForKey:UIImagePickerControllerOriginalImage]);
//        [imgPngData writeToFile:fileSavePath atomically:YES];
//        
//    }
    self.fotoTirada = YES;
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowRespostasNotification" object:nil];
    }];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
