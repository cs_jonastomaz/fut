//
//  RespostaTextoViewController.h
//  fut
//
//  Created by Jonas Tomaz on 3/5/15.
//  Copyright (c) 2015 Jonas Tomaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RespostaTextoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *respostaTextView;

@end
