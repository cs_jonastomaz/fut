//
//  EscolhaDesafioViewController.m
//  fut
//
//  Created by Jonas Tomaz on 3/4/15.
//  Copyright (c) 2015 Jonas Tomaz. All rights reserved.
//

#import "EscolhaDesafioViewController.h"
#import "DetalheDesafioViewController.h"

@implementation EscolhaDesafioViewController

-(void)viewDidLoad
{
    if (self.titleName) {
        self.title = self.titleName;
    }
}

- (IBAction)ShowDesafio:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"ShowDetalheDesafio" sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIButton *button = sender;
    if ([[segue identifier] isEqualToString:@"ShowDetalheDesafio"]) {
        DetalheDesafioViewController *ddvc = (DetalheDesafioViewController *)[segue destinationViewController];
        if (button.tag == 500) {
            ddvc.fromText = YES;
        }
    }
}

@end
